﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Incidents.List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" />
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <article>
        <asp:Panel ID="SearchPanel" runat="server" DefaultButton="btnSearch">
            <asp:Label ID="lblStatus" runat="server" AssociatedControlID="ddlStatus" Text="Статус:"></asp:Label>
            <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
            <br />
            <asp:Label ID="lblName" runat="server" AssociatedControlID="txtName" Text="Название:"></asp:Label>
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblDpts" runat="server" AssociatedControlID="ddlDpts" Text="Отдел:"></asp:Label>
            <asp:DropDownList ID="ddlDpts" runat="server"></asp:DropDownList>
            <br />
            <asp:Label ID="lblDescr" runat="server" AssociatedControlID="txtDescr" Text="Описание:"></asp:Label>
            <asp:TextBox ID="txtDescr" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblWorker" runat="server" AssociatedControlID="ddlWorkers" Text="Работник:"></asp:Label>
            <asp:DropDownList ID="ddlWorkers" runat="server"></asp:DropDownList>
            <br />
            <asp:Label ID="lblCreationDate" runat="server" AssociatedControlID="txtCrDate" Text="Дата создания:"></asp:Label>
            <asp:TextBox ID="txtCrDate" runat="server" CssClass="date"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblCloseDate" runat="server" AssociatedControlID="txtClDate" Text="Дата закрытия:"></asp:Label>
            <asp:TextBox ID="txtClDate" runat="server" CssClass="date"></asp:TextBox>
            <br />
            <asp:Label ID="lblReason" runat="server" AssociatedControlID="txtReason" Text="Причина закрытия:"></asp:Label>
            <asp:TextBox ID="txtReason" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="btnSearch" runat="server" 
                ValidationGroup="SearchGroup" 
                Text="Найти" 
                OnClick="SearchButton_Click" />
        </asp:Panel>
        <hr />
        <asp:Repeater id="searchResultsRepeater" runat="server">
            <HeaderTemplate>
                <table class="dataTable">
                    <caption>Список инцидентов</caption>
                    <thead>
                        <tr>
                            <th class="hiddencol"></th>
                            <th class="status">Статус</th>
                            <th class="name">Название</th>
                            <th class="dpt">Отдел</th>
                            <th class="descr">Описание</th>
                            <th class="worker">Работник</th>
                            <th class="crdate">Дата создания</th>
                            <th class="cldate">Дата закрытия</th>
                            <th class="reason">Причина закрытия</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr>
                            <td class="hiddencol"> <%# DataBinder.Eval(Container.DataItem, "Id") %> </td>
                            <td> <%# DataBinder.Eval(Container.DataItem, "Status") %> </td>
                            <td> <%# DataBinder.Eval(Container.DataItem, "Name") %> </td>
                            <td> <%# DataBinder.Eval(Container.DataItem, "Dpt") %> </td>
                            <td> <%# DataBinder.Eval(Container.DataItem, "Descr") %> </td>
                            <td> <%# DataBinder.Eval(Container.DataItem, "Worker") %> </td>
                            <td> <%# DataBinder.Eval(Container.DataItem, "CrDate") %> </td>
                            <td> <%# DataBinder.Eval(Container.DataItem, "ClDate") %> </td>
                            <td> <%# DataBinder.Eval(Container.DataItem, "Reason") %> </td>
                        </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <br />
        <table class="paginator">
            <tr>
                <td>
                    <asp:PlaceHolder ID="plcPaging" runat="server" />
                    <br />
                    <asp:Label runat="server" ID="lblPageName" />
                </td>
            </tr>
        </table>
    </article>
</asp:Content>

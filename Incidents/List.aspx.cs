﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Incidents
{
    public partial class List : System.Web.UI.Page
    {
        private IncidentsEntities context = new IncidentsEntities();

        protected int RowCount
        {
            get { return (int)ViewState["RowCount"]; }
            set { ViewState["RowCount"] = value; }
        }

        protected int PgSize
        {
            get { return ViewState["PgSize"] != null ? (int)ViewState["PgSize"] : 5; }
            set { ViewState["PgSize"] = value; }
        }

        protected int StatusId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            string status = Request.QueryString["status"];
            if (string.IsNullOrEmpty(status))
            {
                StatusId = 0;
            }
            else
            {
                StatusId = int.Parse(status);
                ddlStatus.SelectedIndex = StatusId;
            }

            if (!IsPostBack)
            {
                PopulateControls();

                FetchData(PgSize, 0);
            }
            else
            {
                plcPaging.Controls.Clear();
                CreatePagingControl();
            }
        }

        protected void FetchData(int take, int pageSize)
        {
            var query = from incident in context.Incidents
                        select incident;

            int statusId = 0;
            int.TryParse(ddlStatus.SelectedValue.ToString(), out statusId);
            if (statusId != 0)
            {
                query = query.Where(item => item.StatusId == statusId);
            }

            if (!string.IsNullOrWhiteSpace(txtName.Text))
            {
                query = query.Where(item => item.Name.Contains(txtName.Text.Trim()));
            }

            int dptId = 0;
            int.TryParse(ddlDpts.SelectedValue.ToString(), out dptId);
            if (dptId != 0)
            {
                query = query.Where(item => item.DptId == dptId);
            }

            if (!string.IsNullOrWhiteSpace(txtDescr.Text))
            {
                query = query.Where(item => item.Description.Contains(txtDescr.Text.Trim()));
            }

            int workerId = 0;
            int.TryParse(ddlWorkers.SelectedValue.ToString(), out workerId);
            if (workerId != 0)
            {
                query = query.Where(item => item.WorkerId == workerId);
            }

            if (!string.IsNullOrWhiteSpace(txtCrDate.Text))
            {
                DateTime creationDate;
                if (DateTime.TryParse(txtCrDate.Text, out creationDate))
                {
                    query = query.Where(item => item.CreationDate == creationDate);
                }
            }

            if (!string.IsNullOrWhiteSpace(txtClDate.Text))
            {
                DateTime closingDate;
                if (DateTime.TryParse(txtClDate.Text, out closingDate))
                {
                    query = query.Where(item => item.ClosingDate == closingDate);
                }
            }

            if (!string.IsNullOrWhiteSpace(txtReason.Text))
            {
                query = query.Where(item => item.Reason.Contains(txtReason.Text.Trim()));
            }

            var query1 = query.Select(i => new
            {
                ID = i.Id,
                Status = i.Statuses.Status,
                Name = i.Name,
                Dpt = i.Departments.Department,
                Descr = i.Description,
                Worker = i.Workers.FullName,
                CrDate = i.CreationDate,
                ClDate = i.ClosingDate,
                Reason = i.Reason,
                Count = query.Count()
            }).OrderBy(item => item.ID).Skip(pageSize).Take(take);

            PagedDataSource pds = new PagedDataSource();
            pds.AllowCustomPaging = true;
            pds.AllowPaging = true;
            pds.DataSource = query1;
            pds.PageSize = PgSize;

            if (!IsPostBack)
            {
                try
                {
                    RowCount = query1.First().Count;
                }
                catch
                {
                    RowCount = 0;
                }

                CreatePagingControl();
            }

            searchResultsRepeater.DataSource = pds;
            searchResultsRepeater.DataBind();
        }

        protected void CreatePagingControl()
        {
            int pageCount = (int)Math.Ceiling(RowCount / (double)PgSize);

            for (int i = 0; i < pageCount; i++)
            {
                LinkButton lnk = new LinkButton();
                lnk.Click += new EventHandler(lbl_Click);
                lnk.ID = "lnkPage" + (i + 1).ToString();
                lnk.Text = (i + 1).ToString();
                plcPaging.Controls.Add(lnk);
                Label spacer = new Label();
                spacer.Text = "&nbsp;";
                plcPaging.Controls.Add(spacer);
            }
        }

        protected void lbl_Click(object sender, EventArgs e)
        {
            LinkButton lnk = sender as LinkButton;
            int currentPage = int.Parse(lnk.Text);
            int take = currentPage * PgSize;
            int skip = currentPage == 1 ? 0 : take - PgSize;

            FetchData(take, skip);
        }

        protected void PopulateControls()
        {
            PopulateStatuses();
            PopulateDepartments();
            PopulateWorkers();
        }

        protected void PopulateStatuses()
        {

            var query = from s in context.Statuses
                        orderby s.Id
                        select new
                        {
                            Id = s.Id,
                            Status = s.Status
                        };

            var list = query.ToList();
            list.Insert(0, new { Id = 0, Status = "Любой" });

            ddlStatus.DataSource = list;
            ddlStatus.DataTextField = "Status";
            ddlStatus.DataValueField = "Id";
            ddlStatus.DataBind();
        }

        protected void PopulateDepartments()
        {
            var query = from d in context.Departments
                        orderby d.Department
                        select new
                        {
                            Id = d.Id,
                            Department = d.Department
                        };

            var list = query.ToList();
            list.Insert(0, new { Id = 0, Department = "Любой" });

            ddlDpts.DataSource = list;
            ddlDpts.DataTextField = "Department";
            ddlDpts.DataValueField = "Id";
            ddlDpts.DataBind();
        }

        protected void PopulateWorkers()
        {
            var query = from w in context.Workers
                        orderby w.FullName
                        select new
                        {
                            Id = w.Id,
                            Worker = w.FullName
                        };

            var list = query.ToList();
            list.Insert(0, new { Id = 0, Worker = "Любой" });

            ddlWorkers.DataSource = list;
            ddlWorkers.DataTextField = "Worker";
            ddlWorkers.DataValueField = "Id";
            ddlWorkers.DataBind();
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                FetchData(PgSize, 0);
            }
        }


        public override void Dispose()
        {
            context.Dispose();
            base.Dispose();
        }
    }
}
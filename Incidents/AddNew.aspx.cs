﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Incidents
{
    public partial class AddNew : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                return;
            }
        }

        protected void ButtonSubmin_Click(object sender, EventArgs e)
        {
            using (var context = new IncidentsEntities())
            {
                //TODO: вынести в отдельный класс работу с БД
                
                Incidents incident = new Incidents();
                incident.Name = txtName.Text;
                incident.Description = txtDescription.Text;
                incident.StatusId = 1;
                incident.CreationDate = DateTime.Now;
                context.Incidents.Add(incident);
                
                int result = context.SaveChanges();

                if (result > 0)
                {
                    lblResult.Text = "Данные успешно сохранены";
                    txtName.Text = string.Empty;
                    txtDescription.Text = string.Empty;
                }
                else 
                {
                    lblResult.Text = "Ошибка при сохраннии данных";
                }
                lblResult.Visible = true;
            }
        }
    }
}
﻿<%@ Page Title="Добавление инцидента" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddNew.aspx.cs" Inherits="Incidents.AddNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <article>
        <asp:Label ID="lblName" runat="server" Text="Название:" AssociatedControlID="txtName"></asp:Label>
        <asp:TextBox ID="txtName" runat="server"></asp:TextBox><br/>
        <asp:Label ID="lblDescription" runat="server" Text="Описание:" AssociatedControlID="txtDescription"></asp:Label>
        <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
        <br/>
        <asp:Label ID="lblResult" runat="server" Text="Label" Visible="False"></asp:Label>
        <br/>
        <asp:Button ID="ButtonSubmin" runat="server" Text="Отправить" OnClick="ButtonSubmin_Click" />
    </article>
</asp:Content>
